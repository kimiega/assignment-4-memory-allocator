#include "tests.h"
#include <stdlib.h>

static struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) -
                                 offsetof(struct block_header, contents));
}
void test_failed() {
    log_error("\n------------- TEST FAILED ------------\n\n");
}
void heap_free(void* heap) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = 10000}).bytes);
}
void normal_successful_memory_allocation_test() {
    log_info("\n------------------- TEST 1 -------------------\n");
    log_info("\n      Normal successful memory allocation\n");
    log_info("\n----------------------------------------------\n\n");
    void* heap = heap_init(10000);
    if (!heap) {
        log_error("heap initialization error\n");
        return test_failed();
    }
    else
        log_info("heap initialized\n");
    debug_heap(stdout, heap);
    void *block1 = _malloc(1000);
    if (block1)
        log_info("<block1> allocated\n");
    else {
        log_error("<block1> allocation error\n");
        heap_free(heap);
        return test_failed();
    }
    debug_heap(stdout, heap);
    _free(block1);
    log_info("<block1> freed\n");
    debug_heap(stdout, heap);
    heap_free(heap);
    log_info("\n------------ TEST 1 PASSED -----------\n\n");
}

void freeing_one_block_from_multiple_allocations_test() {
    log_info("\n------------------- TEST 2 -------------------\n");
    log_info("\n Freeing one block from multiple allocations\n");
    log_info("\n----------------------------------------------\n\n");
    void *heap = heap_init(3228);
    if (!heap) {
        log_error("heap initialization error\n");
        return test_failed();
    }
    else
        log_info("heap initialized\n");
    debug_heap(stdout, heap);
    void *block1 = _malloc(322);
    if (block1)
        log_info("<block1> allocated\n");
    else {
        log_error("<block1> allocation error\n");
        heap_free(heap);
        return test_failed();
    }
    void *block2 = _malloc(228);
    if (block2)
        log_info("<block2> allocated\n");
    else {
        log_error("<block2> allocation error\n");
        _free(block1);
        heap_free(heap);
        return test_failed();
    }
    void *block3 = _malloc(1488);
    if (block3)
        log_info("<block3> allocated\n");
    else {
        log_error("<block3> allocation error\n");
        _free(block1);
        _free(block2);
        heap_free(heap);
        return test_failed();
    }
    debug_heap(stdout, heap);
    _free(block2);
    log_info("<block2> freed\n");
    debug_heap(stdout, heap);
    _free(block1);
    log_info("<block1> freed\n");
    _free(block3);
    log_info("<block3> freed\n");
    heap_free(heap);
    printf("\n------------ TEST 2 PASSED -----------\n\n");
}

void freeing_two_blocks_from_multiple_allocations_test() {
    log_info("\n------------------- TEST 3 -------------------\n");
    log_info("\n Freeing two blocks from multiple allocations\n");
    log_info("\n----------------------------------------------\n\n");
  void *heap = heap_init(3228);
  if (!heap) {
      log_error("heap initialization error\n");
      return test_failed();
  }
  else
      log_info("heap initialized\n");
  debug_heap(stdout, heap);
  void *block1 = _malloc(228);
    if (block1)
        log_info("<block1> allocated\n");
    else {
        log_error("<block1> allocation error\n");
        heap_free(heap);
        return test_failed();
    }
  void *block2 = _malloc(322);
    if (block2)
        log_info("<block2> allocated\n");
    else {
        log_error("<block2> allocation error\n");
        _free(block1);
        heap_free(heap);
        return test_failed();
    }
  void *block3 = _malloc(420);
    if (block3)
        log_info("<block3> allocated\n");
    else {
        log_error("<block3> allocation error\n");
        _free(block1);
        _free(block2);
        heap_free(heap);
        return test_failed();
    }
  void *block4 = _malloc(690);
    if (block4)
        log_info("<block4> allocated\n");
    else {
        log_error("<block4> allocation error\n");
        _free(block1);
        _free(block2);
        _free(block3);
        heap_free(heap);
        return test_failed();
    }
    debug_heap(stdout, heap);
    _free(block4);
    log_info("<block4> freed\n");
    debug_heap(stdout, heap);
    _free(block2);
    log_info("<block2> freed\n");
    debug_heap(stdout, heap);
    _free(block1);
    log_info("<block1> freed\n");
    _free(block3);
    log_info("<block3> freed\n");
    debug_heap(stdout, heap);
    heap_free(heap);
    printf("\n------------ TEST 3 PASSED -----------\n\n");
}

void memory_out_new_memory_region_expands_old_test() {
    log_info("\n------------------- TEST 4 -------------------\n");
    log_info("\n  Memory out, new memory region expands old\n");
    log_info("\n----------------------------------------------\n\n");
    void *heap = heap_init(1000);
    if (!heap) {
        log_error("heap initialization error\n");
        return test_failed();
    }
    else
        log_info("heap initialized\n");
    debug_heap(stdout, heap);
    void *big_boss_block = _malloc(5000);
    if (big_boss_block)
        log_info("<big_boss_block> allocated\n");
    else {
        log_error("<big_boss_block> allocation error\n");
        heap_free(heap);
        return test_failed();
    }
    debug_heap(stdout, heap);
    _free(big_boss_block);
    log_info("<big_boss_block> freed\n");
    debug_heap(stdout, heap);
    heap_free(heap);
    printf("\n------------ TEST 4 PASSED -----------\n\n");
}

void memory_out_new_memory_region_allocates_elsewhere_test() {
    log_info("\n------------------- TEST 5 -------------------\n");
    log_info("\n The memory has run out, the old memory\n");
    log_info("region cannot be expanded due to a different\n");
    log_info("allocated address range,\n");
    log_info("the new region is allocated elsewhere\n");
    log_info("\n----------------------------------------------\n\n");
    void *heap = heap_init(1);
    if (!heap) {
        log_error("heap initialization error\n");
        return test_failed();
    }
    else
        log_info("heap initialized\n");
    debug_heap(stdout, heap);
    void *block1 = _malloc(1024);
    if (block1)
        log_info("<block1> allocated\n");
    else {
        log_error("<block1> allocation error\n");
        heap_free(heap);
        return test_failed();
    }
    debug_heap(stdout, heap);
    struct block_header *header = block_get_header(block1);
    (void)mmap(header->contents + header->capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);
    void *block2 = _malloc(4096);
    if (block2)
        log_info("<block2> allocated\n");
    else {
        log_error("<block2> allocation error\n");
        heap_free(heap);
        return test_failed();
    }
    debug_heap(stdout, heap);
    _free(block1);
    log_info("<block1> freed\n");
    _free(block2);
    log_info("<block2> freed\n");
    munmap(header->next, REGION_MIN_SIZE);
    munmap(heap, REGION_MIN_SIZE);
    printf("\n------------ TEST 5 PASSED -----------\n\n");
}
