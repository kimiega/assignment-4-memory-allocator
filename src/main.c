#include "tests.h"

int main() {
    normal_successful_memory_allocation_test();
    freeing_one_block_from_multiple_allocations_test();
    freeing_two_blocks_from_multiple_allocations_test();
    memory_out_new_memory_region_expands_old_test();
    memory_out_new_memory_region_allocates_elsewhere_test();
  return 0;
}