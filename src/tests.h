#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void normal_successful_memory_allocation_test();
void freeing_one_block_from_multiple_allocations_test();
void freeing_two_blocks_from_multiple_allocations_test();
void memory_out_new_memory_region_expands_old_test();
void memory_out_new_memory_region_allocates_elsewhere_test();

#endif
